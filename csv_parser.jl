using CSV, DataFrames

# read the csv file as a DataFrame
data = CSV.File("input.csv"; select=["beneficiary_name","iban","amount","currency","reference"]) |> DataFrame

function raiseException(col, line, message)
    throw("The file is inconsistent: column '$(col)' $(message) at row $(line)")
end

# loop in all row of the dataFrame
for (i,row) in enumerate(eachrow(data))
    # check if the amount column contains a positive value
    if row[:amount] <= 0
        raiseException("amount", i, "should have a positive value")
    end
    # Qonto only accept 'EUR' as currency
    if row[:currency] != "EUR"
        raiseException("currency", i, "must be 'EUR'")
    end
    # loop in all column names
    for col in names(data)
        # stop the operation if a column is empty and print the error message
        if row[col] === missing
            raiseException(col, i, "is empty")
        end
        # skip the column that is not string type
        if !isa(row[col], String)
            continue
        end
        # replace "  " occurence by " " for each row of each columns
        try
            data[i, col] = replace(row[col], "  " => " ")
        catch e
            # debug
            println(i, e)
        end
    end
end

# overwrite the csv file with data
CSV.write("output.csv", data)

# debug
println(data)