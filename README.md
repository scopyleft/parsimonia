Parse a csv from [Aitable](https://aitable.ai) to export it for [Qonto](https://qonto.com).

## Installation
 
### Prerequirements

- [Julia](https://docs.julialang.org/en/v1/manual/getting-started/)

### Initialize the virtual environment

    git clone https://gitlab.com/scopyleft/parsimonia.git
    cd parsimonia
    julia --project=. -e "using Pkg; Pkg.instantiate()"

## Parse a csv

Create a csv file named input.csv

Initiate the parsing: `julia csv_parser.jl`

The parsed file is output.csv
